# Sulu docker

A docker environment for sulu projects

# Installation

Update or rename `config/webspaces/sulu.xml` file.

Make sure the port 80 is available, then run

```bash
make install
```

Then open [http://sulu.localhost](http://sulu.localhost) to see your app.

