ARG PHP_VERSION=8.0

FROM php:${PHP_VERSION}-fpm
ARG UID=1000
ARG GID=1000

COPY docker/conf/env_provision.sh /usr/local/bin
RUN bash env_provision.sh

COPY docker/conf/install_php.sh /usr/local/bin
RUN bash install_php.sh

COPY docker/conf/install_node.sh /usr/local/bin
RUN bash install_node.sh

RUN usermod -u $UID www-data && \
    groupmod -g $GID www-data && \
    mkdir -p /home/www-data/.composer && \
    chmod 755 /home/www-data/.composer && \
    chown -R www-data:www-data /var/www/ && \
    usermod -d /home/www-data www-data


RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

WORKDIR /var/www/
